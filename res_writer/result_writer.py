import csv
from os import path


class ResultDataObject:

    def __init__(self, winner: str, depth: int, number_of_moves: int, total_time, algorithm: str):
        self.winner = winner
        self.depth = depth
        self.number_of_moves = number_of_moves
        self.total_time = total_time
        self.algorithm = algorithm


def save_data_to_file(result_data: ResultDataObject):
    winner = result_data.winner
    depth = result_data.depth
    number_of_moves = result_data.number_of_moves
    total_time = result_data.total_time
    algorithm = result_data.algorithm
    filename = algorithm
    file_path = "results/" + filename + ".csv"
    is_new_file = path.exists(file_path)
    file = open(file_path, 'a')
    if not is_new_file:
        file.write('winner;depth;number_of_moves;total_time;algorithms\n')
    res_string = winner + ';' + str(depth) + ';' + str(number_of_moves) + ';' + str(total_time) + ';' + algorithm + "\n"
    res_string = str.replace(res_string, '.', ',')
    file.write(res_string)
    file.close()
