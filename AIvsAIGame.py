import time

import numpy as np
import pygame
from pygame.rect import Rect

from AI.BasicAIOponent import *
from AI.MidColumnHeuristic import score_for_mid_column
from Utils.PlayerConstants import AlgorithmConst
from res_writer.result_writer import ResultDataObject


class AIvsAIGame:

    def __init__(self, depth_lvl, first_player_algorithm,
                 second_player_algorithm, with_gui=False, additional_scoring=None):
        self.board = np.zeros((ROW_NUMBER, COLUMN_NUMBER))
        self.is_game_over = False
        self.current_tour = YELLOW_TOUR
        self.depth_lvl = depth_lvl
        self.additional_scoring = additional_scoring
        self.player_one_stone = YELLOW_STONE
        self.player_two_stone = RED_STONE
        self.first_player_algorithm = first_player_algorithm
        self.second_player_algorithm = second_player_algorithm
        self.start_time = 0
        self.with_gui = with_gui

        if self.with_gui:
            pygame.init()
            self.screen = pygame.display.set_mode((WIDTH_GAMEFIELD, HEIGHT_GAMEFIELD))

    def start(self) -> ResultDataObject:
        self.start_time = time.time()
        is_first_move = True

        # GUI
        if self.with_gui:
            self.draw_board()
            pygame.display.update()

        while not self.is_game_over:
            # YELLOW PLAYER MOVE
            first_player_chosen_col = self.get_column(self.first_player_algorithm, self.player_one_stone)

            if is_first_move:
                is_first_move = False
                first_player_chosen_col = random.randint(0, 6)

            is_valid_column = is_valid_col(self.board, first_player_chosen_col)
            if not is_valid_column:
                raise AIException
            self.is_game_over, self.current_tour = \
                make_move(self.board, self.current_tour, first_player_chosen_col)

            # GUI
            if self.with_gui:
                self.draw_board()
                pygame.time.wait(500)

            if self.is_game_over:
                result_data = self.do_game_finished_stuff()
                return result_data

            # RED PLAYER MOVE
            second_player_chosen_col = self.get_column(self.second_player_algorithm, self.player_two_stone)

            is_valid_column = is_valid_col(self.board, second_player_chosen_col)
            if not is_valid_column:
                raise AIException
            self.is_game_over, self.current_tour = \
                make_move(self.board, self.current_tour, second_player_chosen_col)

            # GUI
            if self.with_gui:
                self.draw_board()
                pygame.time.wait(500)

            if len(get_not_full_columns(self.board)) == 0:
                result_data = self.do_game_finished_stuff(True)
                return result_data

        result_data = self.do_game_finished_stuff()
        return result_data

    def get_column(self, algorithm, player):
        opponent = self.player_one_stone if player == self.player_two_stone else self.player_two_stone
        if algorithm == AlgorithmConst.ALPHA_BETA:
            return alpha_beta(self.board, self.depth_lvl, -math.inf, math.inf, True,
                              self.additional_scoring, player, opponent)[0]
        else:
            return minimax(self.board, self.depth_lvl, True, self.additional_scoring, player, opponent)[0]

    def do_game_finished_stuff(self, is_draw=False):
        total_time = time.time() - self.start_time
        first_algorithm = "minimax" if self.first_player_algorithm == AlgorithmConst.MINI_MAX else "alpha_beta"
        second_algorithm = "minimax" if self.second_player_algorithm == AlgorithmConst.MINI_MAX else "alpha_beta"
        heuristic = self.used_heuristic()
        fight = first_algorithm + "_vs_" + second_algorithm + "_" + heuristic
        if is_draw:
            result_data = ResultDataObject('draw', self.depth_lvl, 21, total_time, fight)
            return result_data
        winner, winner_str = (RED_TOUR, "RED") if self.current_tour == YELLOW_TOUR else (YELLOW_TOUR, "YELLOW")
        number_of_moves = np.count_nonzero(self.board == winner)
        result_data = ResultDataObject(winner_str, self.depth_lvl, number_of_moves, total_time, fight)

        # GUI
        if self.with_gui:
            winner, color = ('RED', RED_COLOR) if self.current_tour == YELLOW_TOUR else ('YELLOW', YELLOW_COLOR)
            print('zwyciezca jest ' + winner)
            pygame.time.wait(4000)

        return result_data

    def print_board(self) -> None:
        row_axis = 0
        print(np.flip(self.board, axis=row_axis))

    def used_heuristic(self) -> str:
        if self.additional_scoring is None:
            return "None"
        elif self.additional_scoring is score_for_mid_column:
            return "mid_col_scoring"
        else:
            return "board_center_scoring"

    # GUI
    def draw_board(self) -> None:
        for row_index in range(ROW_NUMBER):
            for col_index in range(COLUMN_NUMBER):
                # draw rectangle
                x_rectangle_pos = col_index * SQUARE_SIZE
                y_rectangle_pos = HEIGHT_GAMEFIELD - (row_index * SQUARE_SIZE + SQUARE_SIZE)
                rectangle_to_draw = Rect(x_rectangle_pos, y_rectangle_pos, SQUARE_SIZE, SQUARE_SIZE)
                pygame.draw.rect(self.screen, BLUE_COLOR, rectangle_to_draw)
                # draw circle
                x_circle_pos = int(col_index * SQUARE_SIZE + SQUARE_SIZE / 2)
                y_circle_pos = HEIGHT_GAMEFIELD - int(row_index * SQUARE_SIZE + SQUARE_SIZE / 2)
                circle_position = (x_circle_pos, y_circle_pos)
                if self.board[row_index][col_index] == YELLOW_TOUR:
                    color = YELLOW_COLOR
                elif self.board[row_index][col_index] == RED_TOUR:
                    color = RED_COLOR
                else:
                    color = BLACK_COLOR
                pygame.draw.circle(self.screen, color, circle_position, RADIUS)
        pygame.display.update()


