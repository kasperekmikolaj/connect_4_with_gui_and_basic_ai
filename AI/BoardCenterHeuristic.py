from Utils.ConstInfo import COLUMN_NUMBER


def score_board_center(board, stone) -> int:
    score = 0

    mid_col_index = COLUMN_NUMBER // 2
    # mid col
    col_array = [int(i) for i in board[:, mid_col_index]]
    col_count = col_array.count(stone)
    score += col_count * 3

    # distance = 1
    col_array = [int(i) for i in board[:, mid_col_index - 1]]
    col_count = col_array.count(stone)
    score += col_count * 2

    col_array = [int(i) for i in board[:, mid_col_index + 1]]
    col_count = col_array.count(stone)
    score += col_count * 2

    # distance = 2
    col_array = [int(i) for i in board[:, mid_col_index - 2]]
    col_count = col_array.count(stone)
    score += col_count * 1

    col_array = [int(i) for i in board[:, mid_col_index + 2]]
    col_count = col_array.count(stone)
    score += col_count * 1

    return score
