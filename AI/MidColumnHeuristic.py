from Utils.ConstInfo import COLUMN_NUMBER


def score_for_mid_column(board, stone) -> int:
    mid_column_index = COLUMN_NUMBER // 2
    center_column = [int(i) for i in list(board[:, mid_column_index])]
    center_count = center_column.count(stone)
    return center_count * 3
