import math
import random
from copy import deepcopy

from Utils.GameUtils import *


class AIException(Exception):
    pass


def alpha_beta(board, depth, alpha, beta, is_maximizing_player, additional_scoring, maximizing_player_stone,
               minimizing_player_stone):
    if is_terminal_node(board):
        if is_player_a_winner(board, maximizing_player_stone):
            return None, 100000000000000
        elif is_player_a_winner(board, minimizing_player_stone):
            return None, -100000000000000
        else:
            return None, 0
    elif depth == 0:
        return None, scoring_function(board, maximizing_player_stone, additional_scoring)
    valid_columns = get_not_full_columns(board)
    chosen_column = random.choice(valid_columns)
    if is_maximizing_player:
        value = -math.inf
        for checking_column_index in valid_columns:
            board_copy = deepcopy(board)
            empty_row_index = get_empty_row_index(board, checking_column_index)
            put_stone_into_col(board_copy, checking_column_index, empty_row_index, maximizing_player_stone)
            _, new_score = alpha_beta(board_copy, depth - 1, alpha, beta, False, additional_scoring,
                                      maximizing_player_stone, minimizing_player_stone)
            if new_score > value:
                value = new_score
                chosen_column = checking_column_index
            # alpha-beta part
            alpha = max(alpha, new_score)
            if alpha >= beta:
                break
        return chosen_column, value
    else:  # minimizing player
        value = math.inf
        for checking_column_index in valid_columns:
            board_copy = deepcopy(board)
            empty_row_index = get_empty_row_index(board, checking_column_index)
            put_stone_into_col(board_copy, checking_column_index, empty_row_index, minimizing_player_stone)
            _, new_score = alpha_beta(board_copy, depth - 1, alpha, beta, True, additional_scoring,
                                      maximizing_player_stone, minimizing_player_stone)
            if new_score <= value:
                value = new_score
                chosen_column = checking_column_index
            # alpha-beta part
            beta = min(beta, new_score)
            if alpha >= beta:
                break
        return chosen_column, value


def minimax(board, depth, is_maximizing_player, additional_scoring, maximizing_player_stone, minimizing_player_stone):
    if is_terminal_node(board):
        if is_player_a_winner(board, maximizing_player_stone):
            return None, 10000000000000
        elif is_player_a_winner(board, minimizing_player_stone):
            return None, -10000000000000
        else:
            return None, 0
    elif depth == 0:
        return None, scoring_function(board, maximizing_player_stone, additional_scoring)
    valid_columns = get_not_full_columns(board)
    chosen_column = random.choice(valid_columns)
    if is_maximizing_player:
        value = -math.inf
        for checking_column_index in valid_columns:
            board_copy = deepcopy(board)
            empty_row_index = get_empty_row_index(board, checking_column_index)
            put_stone_into_col(board_copy, checking_column_index, empty_row_index, maximizing_player_stone)
            _, new_score = minimax(board_copy, depth - 1, False, additional_scoring, maximizing_player_stone,
                                   minimizing_player_stone)
            if new_score > value:
                value = new_score
                chosen_column = checking_column_index
        return chosen_column, value
    else:  # minimizing player
        value = math.inf
        for checking_column_index in valid_columns:
            board_copy = deepcopy(board)
            empty_row_index = get_empty_row_index(board, checking_column_index)
            put_stone_into_col(board_copy, checking_column_index, empty_row_index, minimizing_player_stone)
            _, new_score = minimax(board_copy, depth - 1, True, additional_scoring, maximizing_player_stone,
                                   minimizing_player_stone)
            if new_score < value:
                value = new_score
                chosen_column = checking_column_index
        return chosen_column, value


def scoring_function(board, stone, additional_scoring):
    score = 0
    if additional_scoring is not None:
        score = additional_scoring(board, stone)
    # rows
    for row_index in range(ROW_NUMBER):
        row_array = [int(i) for i in list(board[row_index, :])]
        for column_index in range(COLUMN_NUMBER - 3):
            frame = row_array[column_index:column_index + FRAME_LENGTH]
            score += score_single_frame(frame, stone)
    # columns
    for column_index in range(COLUMN_NUMBER):
        col_array = [int(i) for i in list(board[:, column_index])]
        for row_index in range(ROW_NUMBER - 3):
            frame = col_array[row_index:row_index + FRAME_LENGTH]
            score += score_single_frame(frame, stone)
    # diagonals
    for row_index in range(ROW_NUMBER - 3):
        for column_index in range(COLUMN_NUMBER - 3):
            frame = [board[row_index + i][column_index + i] for i in range(FRAME_LENGTH)]
            score += score_single_frame(frame, stone)
    for row_index in range(ROW_NUMBER - 3):
        for column_index in range(COLUMN_NUMBER - 3):
            frame = [board[row_index + 3 - i][column_index + i] for i in range(FRAME_LENGTH)]
            score += score_single_frame(frame, stone)
    return score


def score_single_frame(frame, stone):
    score = 0
    opp_stone = RED_STONE if stone == YELLOW_STONE else YELLOW_STONE
    if frame.count(stone) == 4:
        score += 10
    elif frame.count(stone) == 3 and frame.count(EMPTY_FIELD) == 1:
        score += 5
    elif frame.count(stone) == 2 and frame.count(EMPTY_FIELD) == 2:
        score += 2
    if frame.count(opp_stone) == 4:
        score -= 5
    elif frame.count(opp_stone) == 3 and frame.count(EMPTY_FIELD) == 1:
        score -= 3
    elif frame.count(opp_stone) == 2 and frame.count(EMPTY_FIELD) == 2:
        score -= 1
    return score
