from Utils.ConstInfo import *


def is_terminal_node(board):
    return is_player_a_winner(board, YELLOW_STONE) or \
           is_player_a_winner(board, RED_STONE) or \
           len(get_not_full_columns(board)) == 0


def put_stone_into_col(board, selected_col_index: int, valid_row_index: int, stone) -> None:
    board[valid_row_index][selected_col_index] = stone


def is_player_a_winner(board, checking_stone) -> bool:
    # check row
    for row_index in range(ROW_NUMBER):
        for col_index in range(COLUMN_NUMBER - 3):
            if board[row_index][col_index] == checking_stone and \
                    board[row_index][col_index + 1] == checking_stone and \
                    board[row_index][col_index + 2] == checking_stone and \
                    board[row_index][col_index + 3] == checking_stone:
                return True
    # check column
    for row_index in range(ROW_NUMBER - 3):
        for col_index in range(COLUMN_NUMBER):
            if board[row_index][col_index] == checking_stone and \
                    board[row_index + 1][col_index] == checking_stone and \
                    board[row_index + 2][col_index] == checking_stone and \
                    board[row_index + 3][col_index] == checking_stone:
                return True
    # check diagonals
    for row_index in range(ROW_NUMBER - 3):
        for col_index in range(COLUMN_NUMBER - 3):
            if board[row_index][col_index] == checking_stone and \
                    board[row_index + 1][col_index + 1] == checking_stone and \
                    board[row_index + 2][col_index + 2] == checking_stone and \
                    board[row_index + 3][col_index + 3] == checking_stone:
                return True
    for row_index in range(3, ROW_NUMBER):
        for col_index in range(COLUMN_NUMBER - 3):
            if board[row_index][col_index] == checking_stone and \
                    board[row_index - 1][col_index + 1] == checking_stone and \
                    board[row_index - 2][col_index + 2] == checking_stone and \
                    board[row_index - 3][col_index + 3] == checking_stone:
                return True


def get_not_full_columns(board):
    top_row_index = ROW_NUMBER - 1
    return [col_index for col_index in range(COLUMN_NUMBER) if board[top_row_index, col_index] == 0]


def get_empty_row_index(board, selected_col_index) -> int:
    for index, elem in enumerate(board[:, selected_col_index]):
        if elem == 0:
            return index


def is_valid_col(board, selected_col_index: int) -> bool:
    return board[ROW_NUMBER - 1][selected_col_index] == 0


def make_move(board, current_tour, selected_col_index: int):
    valid_row_index = get_empty_row_index(board, selected_col_index)
    put_stone_into_col(board, selected_col_index, valid_row_index, current_tour)
    is_game_over = is_player_a_winner(board, current_tour)
    current_tour = RED_TOUR if current_tour == YELLOW_TOUR else YELLOW_TOUR
    return is_game_over, current_tour
