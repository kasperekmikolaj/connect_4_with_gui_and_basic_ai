from AI.BoardCenterHeuristic import score_board_center
from AI.MidColumnHeuristic import score_for_mid_column
from AIvsAIGame import AIvsAIGame
from Connect4Game import Connect4Game
from Utils.PlayerConstants import Constants, AlgorithmConst
from res_writer.result_writer import save_data_to_file

MAX_DEPTH_LVL = 7
NUMBER_OF_GAMES = 20

if __name__ == '__main__':
    game = Connect4Game(True, 5, Constants.YELLOW_PLAYER, score_for_mid_column)
    # game = Connect4Game(True, 5, Constants.RED_PLAYER, score_board_center)
    game.start()
    # game = Connect4Game()
    # game = AIvsAIGame(4, AlgorithmConst.ALPHA_BETA, AlgorithmConst.ALPHA_BETA, False, score_for_mid_column)
    # game = AIvsAIGame(2, AlgorithmConst.ALPHA_BETA, AlgorithmConst.ALPHA_BETA, True, score_board_center)
    # result_data = game.start()
    # save_data_to_file(result_data)

    # alpha_beta vs alpha_beta with None as additional heuristic
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.ALPHA_BETA, False, None)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-a-none')

    # alpha_beta vs alpha_beta with score_for_mid_column
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.ALPHA_BETA, False,
    #                           score_for_mid_column)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-a-mid')

    # alpha_beta vs alpha_beta with score_board_center
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.ALPHA_BETA, False,
    #                           score_board_center)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-a-center')
    #
    # alpha_beta vs minimax with None as additional heuristic
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.MINI_MAX, False, None)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-m-none')
    #
    # alpha_beta vs minimax with score_for_mid_column
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.MINI_MAX, False,
    #                           score_for_mid_column)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-m-mid')
    #
    # alpha_beta vs minimax with score_board_center
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.ALPHA_BETA, AlgorithmConst.MINI_MAX, False,
    #                           score_board_center)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('a-m-center')
    #
    # minimax vs minimax with None as additional heuristic
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.MINI_MAX, AlgorithmConst.MINI_MAX, False, None)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('m-m-none')
    #
    # minimax vs minimax with score_for_mid_column
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.MINI_MAX, AlgorithmConst.MINI_MAX, False, score_for_mid_column)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('m-m-mid')
    #
    # minimax vs minimax with score_board_center
    # for depth_lvl in range(1, MAX_DEPTH_LVL + 1):
    #     total_move_number = 0
    #     total_time = 0
    #     last_result = None
    #     for _ in range(NUMBER_OF_GAMES + 1):
    #         game = AIvsAIGame(depth_lvl, AlgorithmConst.MINI_MAX, AlgorithmConst.MINI_MAX, False, score_board_center)
    #         last_result = game.start()
    #         total_move_number += last_result.number_of_moves
    #         total_time += last_result.total_time
    #
    #     avg_move_number = total_move_number / NUMBER_OF_GAMES
    #     last_result.number_of_moves = avg_move_number
    #
    #     avg_time = total_time / NUMBER_OF_GAMES
    #     last_result.total_time = avg_time
    #     save_data_to_file(last_result)
    #     print(depth_lvl)
    # print('m-m-center')
#