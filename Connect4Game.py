import sys
import numpy as np
import pygame
from pygame.rect import Rect
from AI.BasicAIOponent import *

from Utils.PlayerConstants import Constants


class Connect4Game:

    def __init__(self, play_with_ai: bool = False, depth_lvl: int = 4,
                 ai_player_tour: int = Constants.RED_PLAYER, additional_scoring=None):
        self.board = np.zeros((ROW_NUMBER, COLUMN_NUMBER))
        self.is_game_over = False
        self.current_tour = YELLOW_TOUR
        print(self.board)
        pygame.init()
        self.font = pygame.font.SysFont("comicsansms", 60)
        self.screen = pygame.display.set_mode((WIDTH_GAMEFIELD, HEIGHT_GAMEFIELD))
        self.ai_player_tour = ai_player_tour
        self.ai_stone = ai_player_tour
        self.player_stone = RED_STONE if ai_player_tour == YELLOW_TOUR else YELLOW_TOUR
        self.play_with_ai = play_with_ai
        self.depth_lvl = depth_lvl
        self.additional_scoring = additional_scoring

    def start(self):
        # GUI
        self.draw_board()
        pygame.display.update()

        while not self.is_game_over:
            # GUI
            for game_event in pygame.event.get():
                if game_event.type == pygame.QUIT:
                    sys.exit()

                if game_event.type == pygame.MOUSEMOTION:
                    self.draw_moving_circle(game_event.pos[0])

                if game_event.type == pygame.MOUSEBUTTONDOWN:
                    self.clear_input_row()
                    x_click_position = game_event.pos[0]

                    # game logic
                    selected_col_index = int(math.floor(x_click_position / SQUARE_SIZE))
                    is_valid_column = is_valid_col(self.board, selected_col_index)
                    if not is_valid_column:
                        continue
                    self.is_game_over, self.current_tour = make_move(self.board, self.current_tour, selected_col_index)
                    self.print_board()
                    # GUI
                    self.draw_board()
                    if not self.play_with_ai or not self.current_tour == self.ai_player_tour:
                        self.draw_moving_circle(game_event.pos[0])

                if self.is_game_over:
                    self.do_game_finished_stuff()
                    break

                # AI move
                if self.play_with_ai and self.current_tour == self.ai_player_tour:
                    # AI logic
                    # ai_selected_col_index, _ = \
                    #     minimax(self.board, 4, True, self.additional_scoring, self.ai_stone, self.player_stone)
                    ai_selected_col_index, _ = \
                        alpha_beta(self.board, self.depth_lvl, -math.inf, math.inf, True,
                                   self.additional_scoring, self.ai_stone, self.player_stone)

                    is_valid_column = is_valid_col(self.board, ai_selected_col_index)
                    if not is_valid_column:
                        raise AIException
                    self.is_game_over, self.current_tour = \
                        make_move(self.board, self.current_tour, ai_selected_col_index)
                    self.print_board()
                    # GUI
                    self.draw_board()

                    if self.is_game_over:
                        self.do_game_finished_stuff()
                        break
                    self.draw_moving_circle(350)

                if len(get_not_full_columns(self.board)) == 0:
                    self.is_game_over = True
                    self.do_game_finished_stuff(True)

    def do_game_finished_stuff(self, is_draw=False):
        winner, color = ('RED', RED_COLOR) if self.current_tour == YELLOW_TOUR else ('YELLOW', YELLOW_COLOR)
        print('zwyciezca jest ' + winner)
        self.game_over_gui_part(winner, color, is_draw)

    def print_board(self) -> None:
        row_axis = 0
        print(np.flip(self.board, axis=row_axis))

    # GUI
    def game_over_gui_part(self, winner, color, is_draw):
        self.clear_input_row()
        label = self.font.render('Winner: ' + winner, 1, color)
        if is_draw:
            label = self.font.render('DRAW', 1, BLUE_COLOR)
        self.screen.blit(label, (40, 10))
        pygame.display.update()
        pygame.time.wait(3000)

    # GUI
    def draw_board(self) -> None:
        for row_index in range(ROW_NUMBER):
            for col_index in range(COLUMN_NUMBER):
                # draw rectangle
                x_rectangle_pos = col_index * SQUARE_SIZE
                y_rectangle_pos = HEIGHT_GAMEFIELD - (row_index * SQUARE_SIZE + SQUARE_SIZE)
                rectangle_to_draw = Rect(x_rectangle_pos, y_rectangle_pos, SQUARE_SIZE, SQUARE_SIZE)
                pygame.draw.rect(self.screen, BLUE_COLOR, rectangle_to_draw)

                # draw circle
                x_circle_pos = int(col_index * SQUARE_SIZE + SQUARE_SIZE / 2)
                y_circle_pos = HEIGHT_GAMEFIELD - int(row_index * SQUARE_SIZE + SQUARE_SIZE / 2)
                circle_position = (x_circle_pos, y_circle_pos)
                if self.board[row_index][col_index] == YELLOW_TOUR:
                    color = YELLOW_COLOR
                elif self.board[row_index][col_index] == RED_TOUR:
                    color = RED_COLOR
                else:
                    color = BLACK_COLOR
                pygame.draw.circle(self.screen, color, circle_position, RADIUS)
        pygame.display.update()

    # GUI
    def draw_moving_circle(self, x_position) -> None:
        self.clear_input_row()
        color = YELLOW_COLOR if self.current_tour == YELLOW_TOUR else RED_COLOR
        pygame.draw.circle(self.screen, color, (x_position, int(SQUARE_SIZE / 2)), RADIUS)
        pygame.display.update()

    # GUI
    def clear_input_row(self) -> None:
        pygame.draw.rect(self.screen, BLACK_COLOR, (0, 0, WIDTH_GAMEFIELD, SQUARE_SIZE))
